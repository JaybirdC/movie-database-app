# Engineer II Test
This test is comprised of 5 development related questions and a small code test. For the questions please add as much explanation to your answer as you feel is appropriate.

## Questions
1. Write a function that checks if an inputted value is a palindrome. The function should return true/false (bool). You can assume that all input will be a string type and lower case.

    <?php
        function isPalindrome($input){
            //use strrev() to reverse given input and check that against original
			($input == strrev($input) ? return true : return false);
        }
    ?>

2. Write a function that checks if an inputted value is a numerical range "100-200". Inputted values can be an integer or a string in the previously stated format. The return should be a true/false (bool) value. Ranges should also allow floats (e.g. "100.00"). The range should also be listed as min/max order. Valid values: 100-200, 100.0-200.1. Invalid Values: 100, 200-100.

    <?php
		function checkRange($rangeInput){
			//check if input is integer or numeric string before doing anything
			if (is_numeric($rangeInput[0])){ 
				if($rangeInput == range(100,200.1)) { //This range accommodates ints, floats, and numerical strings
					return true;
				}
            }
            return false;
        }
    ?>


3. What is the difference between a queue and a stack?

    A queue stores data and retrieves it in the order of storage; "First in, first out". A stack likewise stores data, but on retrieval returns the value last stored -- "First in, Last Out" -- rather than first.

4. In an array with integers between 1 and 1,000,000 one value is in the array twice. How do you determine which one?

    $oneMillionArray //assume already populated with values of numbers
    print_r(array_count_values($oneMillionArray)); //print the values contained within the array and the number of each so you can search for which one occurs twice
