<?php
/*This application and all related files in this package were created by and belong to Calvin Marusin*/

//This sub-program modifies a remote list that indicates whether any movie is "owned", first by generating a Session ID and then interacting with the list.

//Grab credentials
require 'api.php';

if($_GET){
	
	/*PART 1: Create Session ID
	***************************/

	//PHASE 1: Generate Request Token
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.themoviedb.org/3/authentication/token/new?api_key=".$apikey,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_POSTFIELDS => "{}",
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		$fixedResponse = json_decode($response); 
		$request_token = $fixedResponse->request_token;
	}

	//PHASE 2: Authenticate Request Token
 
	//Request with Username Url
	$url = 'https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key='.$apikey;

	//The JSON data.
	$jsonData = array(
		'username' => $username,
		'password' => $password,
		"request_token" => $request_token
	);

	//Encode the array into JSON.
	$jsonDataEncoded = json_encode($jsonData);
 
	//Initiate cURL.
	$ch = curl_init($url);
 
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//Set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
	//Tell cURL that we want to send a POST request.
	curl_setopt($ch, CURLOPT_POST, true);
	//Attach our encoded JSON string to the POST fields.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
	//Execute the request
	$json_response = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	$response = json_decode($json_response, true);

	$newResponseToken = $response['request_token'];

	//PHASE 3: Obtain Session ID

	//SessionID URL
	$url = 'https://api.themoviedb.org/3/authentication/session/new?api_key='.$apikey;

	//The JSON data.
	$jsonData = array(
		"request_token" => $newResponseToken
	);

	//Encode the array into JSON.
	$jsonDataEncoded = json_encode($jsonData);

	//Initiate cURL.
	$ch = curl_init($url);
 
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//Set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
	//Tell cURL that we want to send a POST request.
	curl_setopt($ch, CURLOPT_POST, true);
	//Attach our encoded JSON string to the POST fields.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
	//Execute the request
	$json_response = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	curl_close($ch);

	$response = json_decode($json_response, true);

	$sessionID = $response['session_id'];

	$search = $_GET['search'];
	
	/*PART 2: Create Modify List
	***************************/
	
	if($_GET['type'] == "remove"){
		//Type 1: Remove movie from list
	
		//SessionID URL
		$url = 'https://api.themoviedb.org/3/list/'.$list.'/remove_item?api_key='.$apikey.'&session_id='.$sessionID;
	
		//The JSON data.
		$jsonData = array(
			"media_id" => $_GET['id']
		);

		//Encode the array into JSON.
		$jsonDataEncoded = json_encode($jsonData);

		//Initiate cURL.
		$ch = curl_init($url);
 
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		//Tell cURL that we want to send a POST request.
		curl_setopt($ch, CURLOPT_POST, true);
		//Attach our encoded JSON string to the POST fields.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
		//Execute the request
		$json_response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
	
	} elseif ($_GET['type'] == "add") {
		//Type 2: Add movie to list (type included to keep from accidental misfiring)

		//SessionID URL
		$url = 'https://api.themoviedb.org/3/list/'.$list.'/add_item?api_key='.$apikey.'&session_id='.$sessionID;
	
		//The JSON data.
		$jsonData = array(
			"media_id" => $_GET['id']
		);

		//Encode the array into JSON.
		$jsonDataEncoded = json_encode($jsonData);

		//Initiate cURL.
		$ch = curl_init($url);
 
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		//Tell cURL that we want to send a POST request.
		curl_setopt($ch, CURLOPT_POST, true);
		//Attach our encoded JSON string to the POST fields.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
		//Execute the request
		$json_response = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);
		
	}
}

// Redirect browser to main app
header("Location: default.php?search=".urlencode($_GET['search'])."&page=".$_GET['page']); /* Redirect browser */
exit();

?>