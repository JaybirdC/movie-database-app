# The Movie Database API Demo

A practical demonstration of several of the capabilities of TMDB's API. This app queries the online database to retrieve and display a list of movie titles, release dates, overviews, and images. The current app paginates 20 items into a list and can toggle pages to see more. This app also connects to a list on The Movie Database's website that registers whether or not a movie is owned by the user, which can be updated from any search results page.

## Getting Started

In order to load and run this on your own computer, you will need a cURL-enabled XAMPP installation on your machine. Make Word Press core has a quite useful tutorial for installing XAMPP ( https://make.wordpress.org/core/handbook/tutorials/installing-a-local-server/xampp/ ) though this package only requires the Apache server to run cURL.

Once you've installed XAMPP, you must alter its php.ini file to enable cURL. If XAMPP installed correctly, you can find it at “xampp/php/php.ini”. Uncomment the line ";extension=php_curl.dll" by changing it to "extension=php_curl.dll". From now on, every time you activate Apache, cURL will be enabled in your XAMPP installation. (If you made this change while Apache was running, simply restart it by toggling it on and off from the XAMPP control panel).

Now copy this project into "xampp/htdocs". With Apache running, you can now enter "localhost/MovieApp" as a url in your web browser and use the app.

### Prerequisites

You can download XAMPP from the Apache Friends website ( https://www.apachefriends.org/download.html }. Everything else will be included in the XAMPP package and only needs some configuring.

This Demo requires credentials from the TMDB website, which are already included. Those values can be replaced with other credentials, which can be obtained by anyone with a account on TMDB's website. The Add & Remove functionality presupposes an existing list attached to the account providing the api_key.

## Deployment

This system should deploy on any system with Apache and cURL already available.

## Built With

* [The Movie Database API](https://developers.themoviedb.org/3) - The underlying API that queries and processes all information
* [XAMPP](https://www.apachefriends.org/download.html) - Hosting Server
* [HTML5 UP](https://html5up.net) - HTML5 UP Design Template.

## Also

Please read ANSWERS.md for answers to the related questions.

NOTE: The original project description that this was built to satisfy seems to have a slight ambiguity; it wanted users to be able to declare ownership of various titles searched for but there's no corresponding function in the API. A list was built and used for the API to "collect" movies, instead.

## Versioning

I used GIT for versioning.

## Authors

* **Calvin Marusin**

## License

This app is created and owned by Calvin Marusin. It is not authorized for use by other parties. The design was derived from the "Strongly Typed" template created by HTML5 Up.

## Acknowledgments

* TMDB's cool API
* Thanks to Mr. Jess Carlos for pointing me at this API. It was fun to play with.