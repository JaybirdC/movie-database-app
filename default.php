<?php
/*This application and all related PHP files in this package were created by and belong to Calvin Marusin. The HTML and CSS are derived from the HTML5 UP "Strongly Typed" Template */

//Check for search terms in the URL to configure them for the API and to grab them for the title.
(!$_GET ? $searchTerms="" :$searchTerms = str_replace("+", " ", $_GET['search']));
?>
<html>
	<head>
		<title>Movie Database App<?php if ($searchTerms) {echo " -- Movie Search: ".$searchTerms;} ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="movieapp.css">
		<link rel="stylesheet" href="moviesearch.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.css" />
	</head>
	<body class="homepage is-preload">
		<div id="page-wrapper">
		
			<!-- Header -->
				<section id="header">
					<div class="container">

						<!-- Logo -->
							<h1 id="logo"><a href="default.php">Movie Database Demo</a></h1>
							<p>An app built around The Movie Database API</p>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a class="icon fa-home" href="default.php"><span>Home</span></a></li>
									<li>
										<a href="#" class="icon fa-bar-chart-o"><span>Menu Item</span></a>
										<ul>
											<li><a href="#">Lorem ipsum dolor</a></li>
											<li><a href="#">Magna phasellus</a></li>
											<li><a href="#">Etiam dolore nisl</a></li>
											<li>
												<a href="#">Phasellus consequat</a>
												<ul>
													<li><a href="#">Magna phasellus</a></li>
													<li><a href="#">Etiam dolore nisl</a></li>
													<li><a href="#">Phasellus consequat</a></li>
												</ul>
											</li>
											<li><a href="#">Veroeros feugiat</a></li>
										</ul>
									</li>
									<li><a class="icon fa-cog" href="#"><span>Menu Item</span></a></li>
									<li><a class="icon fa-retweet" href="#"><span>Menu Item</span></a></li>
									<li><a class="icon fa-sitemap" href="#"><span>Menu Item</span></a></li>
								</ul>
							</nav>
						
					</div>
<?php
//Load the credentials file and the navbar/search code
require 'api.php';
?>
					<div class="searchbar">
					
<?php require 'search.php'; ?>

					</div>
				</section>
				
				<section id="features">
					<div class="mainbody">
						
						<header>
<?php
//State 1: Default, no Search Terms loaded
if(!(isset($_GET['search']))){
?>
							<h2>Search <strong>The Movie Database</strong> for Your Favorite Movies!</h2>
						</header>
						
<?php
//State 2: Movies List
} else {
	
	//Set the page number to use for pagination purposes and keep your place when interacting with the list
	isset($_GET['page']) ?	$pagenumber = $_GET['page'] : $pagenumber = 1;

	//Generate a list of movies based on the search terms via cURL
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.themoviedb.org/3/search/movie?include_adult=false&page=1&query=".urlencode($searchTerms)."&language=en-US&page=".$pagenumber."&api_key=".$apikey."",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_POSTFIELDS => "{}",
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else  {
		//convert the API results to PHP object
		$fixedResponse = json_decode($response);
		
		$page = $fixedResponse->page;
		
?>
							<h2>Search Results for "<strong><?php echo $searchTerms ?></strong>"</h2>
						</header>
						
						<div class="paginationlinks">	
<?php
		//Generate "Go Back" and "Next Page" links based on current page.
		if ($page > 1) {echo "\t\t\t\t\t\t\t<a id=\"backpage\" href=\"default.php?search=".urlencode($searchTerms)."&page=".($fixedResponse->page-1)."\">Go Back</a>\n";}
		if ($page < $fixedResponse->total_pages) {echo "\t\t\t\t\t\t\t<a id=\"nextpage\" href=\"default.php?search=".urlencode($searchTerms)."&page=".($fixedResponse->page+1)."\">Next Page</a>\n";}
?>
						</div>

						<div class="row aln-center"><?php

		//Begin loading Search results in the app
				
		//Initiate rowcounter, which will create rows for output.
		$rowCounter = 0;

		//Produce list of search results from cURL query
		foreach ($fixedResponse->results as $result) {
			//Grab the movie ID so the app can interact with the list
			$movie_id = $result->id;
		
			//This cURL determines if the movieID is in the specified list.
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.themoviedb.org/3/list/97245/item_status?movie_id=".$movie_id."&api_key=".$apikey,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_POSTFIELDS => "{}",
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				echo "cURL Error #:" . $err;
			} else {
				//convert the API results to PHP object
				$fixedResponse2 = json_decode($response);
				$item_present = $fixedResponse2->item_present;
			}

			//Load options for adding or removing each movie from the list
			$item_present == 1 ? $listText = 'It looks like you own this movie! <a href="list.php?search='.urlencode($searchTerms).'&page='.$page.'&id='.$movie_id.'&type=remove">Don\'t own it?</a>' : $listText = 'You don\'t seem to own this movie. <a href="list.php?search='.urlencode($searchTerms).'&page='.$page.'&id='.$movie_id.'&type=add">Own it?</a>';
		
			//Load nested table with the requisite movie information
			//ENTER SECTION HERE. WRAP IN ROW-DIV, ADD COUNTER AND IF-STATEMENT SO THAT WHEN COUNTER IS DIVISIBLE BY 3, CLOSE AND OPEN NEW ROW
			echo '

								<div class="col-4 col-6-medium col-12-small">
									<!-- Feature -->
										<section class="movieresults">
											<a href="#" class="image featured"><img src="http://image.tmdb.org/t/p/w300'.$result->poster_path.'"/></a>
											<header>
												<h3>'.$result->title.'</h3>
											</header>
											<p>'.$listText.'</p>
											<p class="movieoverview">'.$result->overview.' (Released on '.$result->release_date.').</p>
										</section>

								</div>';
			//Increment the row counter and every third time input a new row.
			$rowCounter += 1;
			if ($rowCounter % 3 == 0) {
				echo '
							
							</div>
							<div class="row aln-center">';
			}
		}
  ?>

							</div>
							<div class="col-12">
								<ul class="actions">
									<li><a href="#" class="button icon fa-file">Tell Me More</a></li>
								</ul>
							</div>
						</div>
					</div>
			
<?php
		//Produce page and result information
		($fixedResponse->total_pages == 1 ? $count = "page" : $count = "pages");
		echo "\t\t\t\t\t<div id=\"paginationinfo\">\n\t\t\t\t\t\t<div id =\"pageinfo\">Page ".$page." of ".$fixedResponse->total_pages." total ".$count.".</div>\n";
		($fixedResponse->total_results == 1 ? $count = "result" : $count = "results");
		echo "\t\t\t\t\t\t<div id =\"resultinfo\">Showing ".count($fixedResponse->results)." of ".$fixedResponse->total_results." total ".$count.".</div>\n\t\t\t\t\t</div>";
	}
?>
<?php
}
?>
				</div>
			</section>
			<div id="cpyrght" class="container">
				<ul class="links">
					<li>&copy; Calvin Marusin, HTML5UP. All rights reserved.</li><li>Design: Jaybird Arts</li>
				</ul>
			</div>
		</div>
	</body>
</html>