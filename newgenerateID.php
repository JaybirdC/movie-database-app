<?php
/*This application and all related files in this package were created by and belong to Calvin Marusin*/

//This sub-program produces a new session ID from TMDB's website, which the API needs to interact with lists

//Grab credentials
require 'api.php';

//PHASE 1: Generate Request Token
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://api.themoviedb.org/3/authentication/token/new?api_key=".$apikey,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS => "{}",
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$fixedResponse = json_decode($response); 
	$request_token = $fixedResponse->request_token;
}

//PHASE 2: Authenticate Request Token
 
//Request with Username Url
$url = 'https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key='.$apikey;

//The JSON data.
$jsonData = array(
	'username' => $username,
	'password' => $password,
    "request_token" => $request_token
);

//Encode the array into JSON.
$jsonDataEncoded = json_encode($jsonData);
 
//Initiate cURL.
$ch = curl_init($url);
 
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, true);
//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
//Execute the request
$json_response = curl_exec($ch);
$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close($ch);

$response = json_decode($json_response, true);

$newResponseToken = $response['request_token'];

//PHASE 3: Obtain Session ID

//SessionID URL
$url = 'https://api.themoviedb.org/3/authentication/session/new?api_key='.$apikey;

//The JSON data.
$jsonData = array(
    "request_token" => $newResponseToken
);

//Encode the array into JSON.
$jsonDataEncoded = json_encode($jsonData);

//Initiate cURL.
$ch = curl_init($url);
 
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, true);
//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
//Execute the request
$json_response = curl_exec($ch);
$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close($ch);

$response = json_decode($json_response, true);

$sessionID = $response['session_id'];

$search = $_GET['search'];

// Redirect browser to main app
header("Location: newdefault.php?search=".$search."&sessionID=".$sessionID); 
exit();
?>